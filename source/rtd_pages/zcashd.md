# Zcash Full Node and CLI

Zcashd & Zcash-cli are the main flagship products maintained by Electric Coin Company (the founding team for Zcash).  A full node downloads a copy of the blockchains and fully enforces all of the rules of the network (while a light client is referencing a trusted full node's copy of the blockchain). 

The zcashd client is used to run a full node client on the Zcash network, and the zcash-cli allows scripted interactions with the zcashd client (for instance, to tell it to send a transaction).


## Installation

* [User Guide](user_guide.html)
* [Debian Packages Setup](install_debian_bin_packages.html)
* [Binary Tarball Download and Setup](install_binary_tarball.html)

## References
* [Troubleshooting Guide](troubleshooting_guide.html)
* [Zcash Payment API](payment_api.html)
* [Wallet Backup Instructions](wallet_backup.html)
* [Sending Memos with Zcashd](memos.html)
* [Zcash.conf guide](zcash_conf_guide.html)
* [Zcash Mining guide](zcash_mining_guide.html)
* [Security Warnings](security_warnings.html)
* [Data Directory Files](files.html)
* [Tor Support in zcashd](tor.html)